from django.conf.urls import patterns, include, url
from educationalmodels.views import *
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:

    url(r'^$', 'educationalmodels.views.default_page', name='home'),
    url(r'^suggestion$', 'educationalmodels.views.get_value'),
    url(r'^result$', 'educationalmodels.views.get_school'),
    url(r'^graph$', 'educationalmodels.views.piechart'),
    #url(r'(?P<district>\w{0,15})/(?P<vdc>\w{0,25})/(?P<ward>\d{0,2})',getSchoolData),
    # url(r'^schoolSearchNepal/', include('schoolSearchNepal.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
