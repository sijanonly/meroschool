# Create your views here.
from educationalmodels.models import *
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
import json
from django.views.decorators.csrf import csrf_exempt
import random
@csrf_exempt
def default_page(request):
	return render_to_response("result.html")
@csrf_exempt
def get_value(request):
	if request.method == 'POST':
		if request.POST['get'] == 'district':
			'''This method returns district Name'''
			district = District.manager.filter(district_name__startswith = request.POST['district'].capitalize())
			districtName = []
			for eachDistrict in district:
				districtName.append(eachDistrict.district_name)
			return HttpResponse(json.dumps({'result': districtName}))

		if request.POST['get'] == 'vdc':
			'''This method returns Vdc Names'''
			vdc = Vdc.manager.filter(vdc_name__startswith=request.POST['vdc'].capitalize(), district__district_name = request.POST['district'].capitalize())
			vdcName = []
			for eachVdc in vdc:
				vdcName.append(eachVdc.vdc_name)
			return HttpResponse(json.dumps({'result':vdcName}))

		if request.POST['get'] == 'ward':
			'''This method returns ward Numbers'''
			ward = Address.manager.values_list('ward_no', flat=True).filter(district__district_name = request.POST['district'].capitalize(), vdc__vdc_name = request.POST['vdc'].capitalize())
			return HttpResponse(json.dumps({'result':set(ward)}))
@csrf_exempt
def get_school(request):
	'''This method returns school list'''
	if request.POST['get'] == 'district':
		schools = School.manager.filter(address__district__district_name = request.POST['district'].capitalize())
		result = []
		sortedSchools = sort(1, schools)
		for eachSchool in sortedSchools:
			eachSchool = eachSchool[0]
			result.append({'schoolName':eachSchool.school_name, 'address': (eachSchool.address.district.district_name + " " + eachSchool.address.vdc.vdc_name), 'estDate':eachSchool.establish_date})
		topFiveSort = sortedSchools[0:5]
		retVal =[]
		for eachTopSchool in topFiveSort:
			retVal.append([eachTopSchool[0].school_name, eachTopSchool[1]])
		#piechart(retVal)
		return HttpResponse(json.dumps({'result':result}))
		

	if request.POST['get'] == 'vdc':
		schools = School.manager.filter(address__district__district_name = request.POST['district'].capitalize(), address__vdc__vdc_name=request.POST['vdc'].capitalize())
		result = []
		sortedSchools = sort(request.POST['sort_order'], schools)
		for eachSchool in sortedSchools:
			result.append({'schoolName':eachSchool.school_name, 'address': eachSchool.address.district.district_name + " " + eachSchool.address.vdc.vdc_name + " " + eachSchool.address.ward_no, 'estDate':school.establish_date})
		return HttpResponse(json.dumps({'result':result}))
		

	if request.POST['get'] == 'ward':
		schools = School.manager.filter(address__district__district_name = request.POST['district'].capitalize(), address__vdc__vdc_name=request.POST['vdc'].capitalize(), address__ward_no = int(request.POST['ward']))
		result = []
		sortedSchools = sort(request.POST['sort_order'], schools)
		for eachSchool in sortedSchools:
			result.append({'schoolName':eachSchool.school_name, 'address': eachSchool.address.district.district_name + " " + eachSchool.address.vdc.vdc_name + " " + eachSchool.address.ward_no, 'estDate':school.establish_date})
		return HttpResponse(json.dumps({'result':result}))
@csrf_exempt	
def pieData(request):
	'''This method returns school list'''
	if request.POST['get'] == 'district':
		schools = School.manager.filter(address__district__district_name = request.POST['district'].capitalize())
		sortedSchools = sort(request.POST['sort_order'], schools)
		topFiveSort = sortedSchools[0:5]
		pieVal =[]
		for eachTopSchool in topFiveSort:
			pieVal.append([eachTopSchool.school_name, eachTopSchool[1]])
		return pieVal

	if request.POST['get'] == 'vdc':
		schools = School.manager.filter(address__district__district_name = request.POST['district'].capitalize(), address__vdc__vdc_name=request.POST['vdc'].capitalize())
		sortedSchools = sort(request.POST['sort_order'], schools)
		topFiveSort = sortedSchools[0:5]
		pieVal =[]
		for eachTopSchool in topFiveSort:
			pieVal.append([eachTopSchool.school_name, eachTopSchool[1]])
		return pieVal

	if request.POST['get'] == 'ward':
		schools = School.manager.filter(address__district__district_name = request.POST['district'].capitalize(), address__vdc__vdc_name=request.POST['vdc'].capitalize(), address__ward_no = int(request.POST['ward']))
		sortedSchools = sort(request.POST['sort_order'], schools)
		topFiveSort = sortedSchools[0:5]
		pieVal =[]
		for eachTopSchool in topFiveSort:
			pieVal.append([eachTopSchool.school_name, eachTopSchool[1]])
		return pieVal

@csrf_exempt		
def sort(sort_val, schoolParams):
	if sort_val==1:
		'''Sort ob basis of STR'''
		schoolList = []
		for school in schoolParams:
			teacherNum =0
			studentNum =0			
			schId = school.id

			teacher = Teacher.manager.filter(school_id = schId)	
			student = Grade.manager.filter(school_id=schId)
			for eachTeacher in teacher:
				teacherNum += eachTeacher.male_teacher + eachTeacher.female_teacher

			for eachStu in student:
				studentNum += eachStu.num_enroll_girl + eachStu.num_enroll_boy

			if teacherNum != 0:
				schoolList.append([school, int(studentNum/teacherNum)])
		print teacherNum, studentNum
		return sorted(schoolList, key=lambda tup:tup[1])


	if sort_val==2:
		#Sort on basis of SCR
		pass
@csrf_exempt
def piechart(request):
	if request.POST['get'] == 'district':
		schools = School.manager.filter(address__district__district_name = request.POST['district'].capitalize())
		result = []
		sortedSchools = sort(1, schools)
		for eachSchool in sortedSchools:
			eachSchool = eachSchool[0]
			result.append({'schoolName':eachSchool.school_name, 'address': (eachSchool.address.district.district_name + " " + eachSchool.address.vdc.vdc_name), 'estDate':eachSchool.establish_date})
		topFiveSort = sortedSchools[0:5]
		retVal =[]
		for eachTopSchool in topFiveSort:
			retVal.append([eachTopSchool[0].school_name, eachTopSchool[1]])
		#piechart(retVal)
	

	schoolDetail = retVal
	xdata = []
	ydata = []

	for eachData in schoolDetail:
		xdata.append(eachData[0])
		ydata.append(eachData[1])
	nb_element = 5
	#ydata = [12,34,53,23,36]
	chartdata = {'x':xdata, 'y':ydata}
	charttype = "pieChart"
	data = {

		'charttype' :charttype,
		'chartdata' :chartdata
	}
	print data
	return render_to_response('piechart.html', data)
