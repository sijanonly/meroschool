var dxcord = 84.4; var dycord = 28.48; var mapzoomlevel=6;

var map = L.map('map').setView([dycord, dxcord],mapzoomlevel); //28.48, 84.4], 7);

		L.tileLayer('http://{s}.tile.cloudmade.com/{key}/22677/256/{z}/{x}/{y}.png', {
			attribution: 'Educational Data Mining, Pulchwok Campus, 2013',
			key: 'BC9A493B41014CAABB98F0471D759707'
		}).addTo(map);

		var baseballIcon = L.icon({
			iconUrl: 'baseball-marker.png',
			iconSize: [32, 37],
			iconAnchor: [16, 37],
			popupAnchor: [0, -28]
		});
var dist = {};

var lastClickedDistrict = null;

function gotodistrict(e) {
	if(lastClickedDistrict)
	{
		lastClickedDistrict.setStyle({
        fillColor: '#f03',
        weight: 2,
        opacity: 1,
        color: 'blue',
        dashArray: '3',
        fillOpacity: 0.7
    });
	}
			var layer = e.target;
lastClickedDistrict = layer;
			layer.setStyle({
				fillColor: 'blue',
				weight: 5,
				color: '#666',
				dashArray: '',
				fillOpacity: 0.7
			});

			if (!L.Browser.ie && !L.Browser.opera) {
				//layer.bringToFront();
			}

			info.update(layer.feature.properties);
			var districtname = (layer.feature.properties.Name).toLowerCase();

			districtname = districtname.charAt(0).toUpperCase() + districtname.slice(1);
			searchbylocation(districtname);
		}



		var info = L.control();


		info.onAdd = function (map) {
			this._div = L.DomUtil.create('div', 'info');
			this.update();
			return this._div;
		};

		info.update = function (props) {
			this._div.innerHTML = '<h4>District</h4>' +  (props ?
				'<b>' + props.Name : 'Click on a district to find Schools');
		};

		info.addTo(map);

function highlightFeature(e) {
			var layer = e.target;
			if (!L.Browser.ie && !L.Browser.opera) {
				//layer.bringToFront();
			}

			info.update(layer.feature.properties);
		}



		

		function onEachFeature(feature, layer) {
			var popupContent = "<p>";
			var i=1;
			
			popupContent +="</p>";

			layer.on({

				mouseover: highlightFeature,
				click: gotodistrict
			});


		}
	
		
function style(feature) {
	
    return {
        fillColor: '#f03',
        weight: 2,
        opacity: 1,
        color: 'blue',
        dashArray: '3',
        fillOpacity: 0.7
    };
}

geojson = L.geoJson(districtmap, {
			style: style,
			onEachFeature: onEachFeature
		}).addTo(map);




