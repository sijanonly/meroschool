from django.db import models
from educationalmodels.manager import *

# Create your models here.

from django.db import models

# Create your models here.
class districtmap(models.Model):
	district_name = models.CharField(max_length=255)
	x=models.CharField(max_length=255)
	y=models.CharField(max_length=255)
	coordinates=models.TextField()
	manager = VdcMapManager()

class vdcmap(models.Model):
	vdc_name = models.CharField(max_length=255)
	district = models.CharField(max_length=255)
	zone=models.CharField(max_length=255)
	eco_name=models.CharField(max_length=255)
	x=models.CharField(max_length=255)
	y=models.CharField(max_length=255)
	coordinates=models.TextField()
	manager = VdcMapManager()

class Anchal(models.Model):
	anchal_name = models.CharField(max_length=50)
	manager = AnchalManager()

class District(models.Model):
	district_code = models.CharField(max_length=30)
	district_name = models.CharField(max_length=50)
	anchal = models.ForeignKey(Anchal)
	manager = DistrictManager()

class Vdc (models.Model):
	vdc_name = models.CharField(max_length=30)
	vdc_code = models.CharField(max_length=30)
	district = models.ForeignKey(District)
	manager = VdcManager()

class Address(models.Model):
	district = models.ForeignKey(District)
	vdc = models.ForeignKey(Vdc)
	ward_no = models.IntegerField()
	location = models.TextField()
	manager = AddressManager()




class School(models.Model):
	year = models.IntegerField()
	school_code = models.IntegerField(max_length=30,unique=True)
	school_name = models.CharField(max_length=100)
	address = models.ForeignKey(Address)
	establish_date = models.CharField(max_length=50)
	school_level = models.CharField(max_length=50)
	school_type = models.CharField(max_length=50)
	number_of_class = models.IntegerField()
	manager = SchoolManager()



class Infrastructure(models.Model):
	year = models.IntegerField()
	school = models.ForeignKey(School)
	girls_toilet = models.IntegerField()
	boys_toilet = models.IntegerField()
	teachers_toilet = models.IntegerField()
	drinking_water_facility = models.BooleanField()
	total_classroom_pakki = models.IntegerField()
	total_classroom_kachhi = models.IntegerField()
	manager = InfrastructureManager()


class Teacher(models.Model):
	year = models.IntegerField()
	school = models.ForeignKey(School)
	teacher_level = models.IntegerField()
	teacher_type = models.CharField(max_length=50)
	male_teacher = models.IntegerField()
	female_teacher = models.IntegerField()
	#female_teacher = models.IntegerField()
	#male_teacher = models.IntegerField()
	#untrained_teacher = models.IntegerField()
	#trained_teacher = models.IntegerField()
	#total_teacher = models.IntegerField()
	#approved_teacher
	manager = TeacherManager()



class Outcome(models.Model):
	year = models.IntegerField()
	school = models.ForeignKey(School)
	#caste_children = models.IntegerField()
	#tribe_children = models.IntegerField()
	#num_enroll_girl_dalit = models.IntegerField()
	#num_enroll_girl_janjati = models.IntegerField()
	#num_enroll_boy_dalit = models.IntegerField()
	#num_enroll_boy_janjati = models.IntegerField()
	manager = OutcomeManager()




class Grade(models.Model):
	grade = models.IntegerField()
	year = models.IntegerField()
	school = models.ForeignKey(School)
	#num_boy = models.IntegerField()
	#num_girl = models.IntegerField()
	num_enroll_girl = models.IntegerField()
	num_enroll_boy = models.IntegerField()
	num_dropout_boy = models.IntegerField()
	num_dropout_girl = models.IntegerField()
	num_promote_girl = models.IntegerField()
	num_promote_boy =  models.IntegerField()
	num_repeat_girl = models.IntegerField()
	num_repeat_boy = models.IntegerField()
	num_enroll_girl_dalit = models.IntegerField()
	num_enroll_girl_janjati = models.IntegerField()
	num_enroll_boy_dalit = models.IntegerField()
	num_enroll_boy_janjati = models.IntegerField()
	manager = GradeManager()


	
#Educational Development Index models

# class Parameter(models.Model):
# 	api_code = models.CharField(max_length=100)
# 	full_name = models.CharField(max_length=150)
# 	matrix = models.CharField(max_length=50)

# class ParameterValue(models.Model):
# 	district = models.ForeignKey(District)
# 	parameter = models.ForeignKey(Parameter)
# 	year = models.IntegerField()
# 	value = models.FloatField()

